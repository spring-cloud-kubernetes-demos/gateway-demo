package com.thinksky.api.gateway.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class Config {

    @Bean
    @LoadBalanced
    WebClient makeWebClient() {
        return WebClient
                .builder()
                .build();
    }


    @Bean
    public RouteLocator customRouteLocator(RouteLocatorBuilder builder) {
        return builder.routes()
                .route("get_httpbin", r -> r.path("/get")
                        .uri("http://httpbin.org"))
                .route("get_person", r -> r.path("//personservice/**")
                        .uri("http://person-service-demo-service:8787"))
                .build();
    }

}
